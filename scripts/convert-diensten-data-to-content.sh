convertData () {
    BASENAME=$(basename -- "$1")
    CONTENTFILENAME=./content/dienst/${BASENAME%.info.json}.md
    DATE=$(jq -r .upload_date "$1")
    echo $CONTENTFILENAME
    if [ ! -f "$CONTENTFILENAME" ]; then 
        echo "+++
title = "$(jq .title "$1")"
date = ${DATE:0:4}-${DATE:4:2}-${DATE:6:2}
videos = [\"https://www.youtube.com/embed/$(jq -r .id "$1")\"]
+++
$(jq -r .description "$1")" >> "$CONTENTFILENAME"
    fi
}

find ./data/diensten -type f | while read file; do convertData "$file"; done