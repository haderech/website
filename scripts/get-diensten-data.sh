#!/bin/sh
getChannelUrl () {
    echo $(grep youtubeurl ./config.toml | cut -d = -f 2 | tr -d '[="=] [:blank:]')/videos
}

writeVideoMetadata () {
    youtube-dl --quiet --no-overwrites --skip-download --write-info-json "$1"
}

filterVideoMetadata () {
    for file in ./data/diensten/*; do
    echo $(jq '{id, title, description, upload_date, thumbnail}' "./$file") > "$file";
    # if 
    done;
}

writeVideoMetadata $(getChannelUrl)
filterVideoMetadata